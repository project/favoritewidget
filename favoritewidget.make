core = 7.x
api = 2

libraries[jquery.favoritewidget][download][type] = "file"
libraries[jquery.favoritewidget][download][url] = "https://github.com/UH-StudentServices/jquery.favoritewidget/archive/v1.1.0.zip"
