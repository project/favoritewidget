<?php

/**
 * @file
 * Implementation of the Favorite Widget Drupal module´ theme callbacks.
 */

/**
 * Preprocess callback for 'favorite_widget'.
 *
 * @param array $variables
 *   An associative array containing:
 *   - add_to_favorites_label: Add label.
 *   - delete_from_favorites_label: Remove label.
 */
function template_preprocess_favorite_widget(&$variables) {

  // Load required JS libraries.
  drupal_add_library('system', 'jquery.cookie');
  drupal_add_library('system', 'ui.widget');
  libraries_load('jquery.favoritewidget');

  // Specify settings.
  $variables['add_to_favorites_label'] = t('Add to favorites');
  $variables['delete_from_favorites_label'] = t('Remove from favorites');

}

/**
 * Theme callback for 'favorite_widget'.
 *
 * @param array $variables
 *   An associative array containing:
 *   - node: Node object.
 *   - add_to_favorites_label: Add label.
 *   - delete_from_favorites_label: Remove label.
 *
 * @return string
 *   HTML div tag.
 */
function theme_favorite_widget($variables) {
  $node = $variables['node'];
  $add_to_favorites_label = $variables['add_to_favorites_label'];
  $delete_from_favorites_label = $variables['delete_from_favorites_label'];

  $data = '(function($){
    $(".favorite-widget.favorite-widget--nid--' . $node->nid . '")
      .favoriteWidget({
        id: ' . $node->nid . ',
        addToFavLabel: "' . $add_to_favorites_label . '",
        delFromFavLabel: "' . $delete_from_favorites_label . '"
    });
  })(jQuery)
  ';
  $options = array(
    'type' => 'inline',
    'scope' => 'footer',
    'group' => JS_DEFAULT,
    'requires_jquery' => TRUE,
  );
  drupal_add_js($data, $options);
  return '<div class="favorite-widget favorite-widget--nid--' . $node->nid . '"></div>';
}
