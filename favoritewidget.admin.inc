<?php

/**
 * @file
 * Implementation of the Favorite Widget Drupal module´ admin configuration.
 */

/**
 * Settings form for the module.
 */
function favoritewidget_admin_settings($form, $form_state) {
  $form['favoritewidget'] = array(
    '#type' => 'fieldset',
    '#title' => t('Favorite Widget settings'),
  );
  $node_types = node_type_get_types();
  $options = array();
  foreach ($node_types as $node_type) {
    $options[$node_type->type] = $node_type->name;
  }
  $form['favoritewidget']['favoritewidget_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('Select content types that you wish to embed favorite widgets.'),
    '#options' => $options,
    '#default_value' => variable_get('favoritewidget_node_types', array()),
  );

  $form['#submit'][] = 'favoritewidget_admin_settings_clear_caches';
  return system_settings_form($form);
}

/**
 * On form submissions field displays should get cache reset as new potential,
 * because extra field has appeared or disappeared.
 */
function favoritewidget_admin_settings_clear_caches($form, $form_state) {
  field_info_cache_clear();
}
